# PrimerAppTornado


## Descripciòn

Esta es una aplicacion sencilla que utiliza el framework Tornado para python3, cuando se ejecuta puede imprimir un hola mundo o la suma de dos numeros entrando al localhost:3000 dentro del navegador

## Requisitos de instalacion

- python3
- Tornado framework

## Ejecucion

- En la terminal, acceder a la carpeta donde este codigo
- Ejecutar el comando "python3 app.py" (para ubuntu)
- Entrar al navegador y buscar "http://localhost:3000/" para ver el mensaje "Hola Mundo"
- Para realiza la suma de dos numeros se debe buscar como "http://localhost:3000/sum?num1=PRIMERNUMERO&num2=SEGUNDONUMERO", reemplazando "PRIMERNUMERO" y "SEGUNDONUMERO" con los numeros que se desean sumar


## Autores

- Angel Eduardo Garibay Valenzuela, matricula: 348775
- Jair Alejandro Gaytan Espíndola, matricula: 353205
