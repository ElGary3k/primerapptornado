import tornado.ioloop
import tornado.web

class HMundo(tornado.web.RequestHandler):
    def get(self):
        self.write("Hola Mundo")

class Suma(tornado.web.RequestHandler):
    def get(self):
        num1 = self.get_argument("num1")
        num2 = self.get_argument("num2")
        try:
            num1 = float(num1)
            num2 = float(num2)
            result = num1 + num2
            self.write("La suma de {} y {} es {}".format(num1, num2, result))
        except ValueError:
            self.write("Envia solo numeros, porfa")

def make_app():
    return tornado.web.Application([
        (r"/", HMundo),
        (r"/sum", Suma),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(3000)
    tornado.ioloop.IOLoop.current().start()

